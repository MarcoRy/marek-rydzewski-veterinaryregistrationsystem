package com.mr.registration.controller;

import com.mr.registration.domain.Owner;
import com.mr.registration.domain.OwnerDto;
import com.mr.registration.domain.PatientDto;
import com.mr.registration.mapper.OwnerMapper;
import com.mr.registration.mapper.PatientMapper;
import com.mr.registration.service.DbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/a1/owner/")
public class OwnerController {
    @Autowired
    DbService dbService;
    @Autowired
    OwnerMapper ownerMapper;
    @Autowired
    PatientMapper patientMapper;

    @RequestMapping(method = RequestMethod.GET, value = "getOwners")
    public List<OwnerDto> getOwners() {
        return ownerMapper.mapToOwnerDtoList(dbService.getAllOwners());
    }

    @RequestMapping(method = RequestMethod.GET, value = "getOwner")
    public OwnerDto getOwner(@RequestParam Long ownerId) throws OwnerNotFoundException {
        return ownerMapper.mapToOwnerDto(dbService.getOwner(ownerId).orElseThrow(OwnerNotFoundException::new));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "unregisterOwner")
    public void unregisterOwner(@RequestParam Long ownerId) {
        dbService.unregisterOwner(ownerId);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "updateOwner", consumes = APPLICATION_JSON_VALUE)
    public OwnerDto updateOwner(@RequestBody OwnerDto ownerDto) {
        return ownerMapper.mapToOwnerDto(dbService.registerOwner(ownerMapper.mapToOwner(ownerDto)));
    }

    @RequestMapping(method = RequestMethod.POST, value = "registerOwner", consumes = APPLICATION_JSON_VALUE)
    public void registerOwner(@RequestBody OwnerDto ownerDto) {
        dbService.registerOwner(ownerMapper.mapToOwner(ownerDto));
    }





    @RequestMapping(method = RequestMethod.POST, value = "registerOwnerWithAnimal", consumes = APPLICATION_JSON_VALUE)
    public void registerOwnerWithAnimal(@RequestBody PatientDto patientDto, @RequestBody OwnerDto ownerDto){
        dbService.registerOwner(ownerMapper.mapToOwner(ownerDto)).getPatients().add(dbService.registerPatient(patientMapper.mapToPatient(patientDto)));
        dbService.registerPatient(patientMapper.mapToPatient(patientDto)).setOwner( dbService.registerOwner(ownerMapper.mapToOwner(ownerDto)));
    }
}
