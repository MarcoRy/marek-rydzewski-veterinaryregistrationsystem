package com.mr.registration.controller;

import com.mr.registration.domain.OwnerAndAnimal;
import com.mr.registration.domain.PatientDto;
import com.mr.registration.mapper.PatientMapper;
import com.mr.registration.service.DbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/a1/patient")
public class PatientController {
    @Autowired
    private DbService service;
    @Autowired
    private PatientMapper patientMapper;

    @RequestMapping(method = RequestMethod.GET, value = "getPatients")
    public List<PatientDto> getPatients() {
        return patientMapper.mapToPatientDtoList(service.getAllPatients());
    }

    @RequestMapping(method = RequestMethod.GET, value = "getPatient")
    public PatientDto getPatient(@RequestParam Long patientId) throws PatientNotFoundException {
        return patientMapper.mapToPatientDto(service.getPatient(patientId).orElseThrow(PatientNotFoundException::new));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "unregisterPatient")
    public void unregisterPatien(@RequestParam Long patientId) {
        service.unregisterPatient(patientId);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "updatePatient")
    public PatientDto updatePatient(@RequestBody PatientDto patientDto) {
        return patientMapper.mapToPatientDto(service.registerPatient(patientMapper.mapToPatient(patientDto)));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "addOwnerToAnimal")
    public void addOwnerToAnimal( @RequestBody PatientDto patientDto) throws PatientNotFoundException, OwnerNotFoundException {
        service.getPatient(patientDto.getId()).orElseThrow(PatientNotFoundException::new).setOwner(service.getOwner(patientDto.getOwnerId()).orElseThrow(OwnerNotFoundException::new));
//        registerPatient(patientDto);
    }

    @RequestMapping(method = RequestMethod.POST, value = "registerPatient", consumes = APPLICATION_JSON_VALUE)
    public void registerPatient(@RequestBody PatientDto patientDto) {
        service.registerPatient(patientMapper.mapToPatient(patientDto));
    }
}
