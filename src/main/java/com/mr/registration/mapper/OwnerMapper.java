package com.mr.registration.mapper;

import com.mr.registration.domain.Owner;
import com.mr.registration.domain.OwnerDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OwnerMapper {

    public Owner mapToOwner(OwnerDto ownerDto) {
        return new Owner(
                ownerDto.getId(),
                ownerDto.getFirstName(),
                ownerDto.getLastName(),
                ownerDto.getAddress(),
                ownerDto.getTelefonNumber()
        );
    }

    public OwnerDto mapToOwnerDto(Owner owner) {
        return new OwnerDto(
                owner.getId(),
                owner.getFirstName(),
                owner.getLastName(),
                owner.getAddress(),
                owner.getTelefonNumber()
        );
    }

    public List<OwnerDto> mapToOwnerDtoList(List<Owner> ownersList) {
        return ownersList.stream()
                .map(t -> new OwnerDto(t.getId(), t.getFirstName(), t.getLastName(), t.getAddress(), t.getTelefonNumber()))
                .collect(Collectors.toList());

    }
}
