package com.mr.registration.mapper;

import com.mr.registration.domain.Patient;
import com.mr.registration.domain.PatientDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PatientMapper {
    public Patient mapToPatient(final PatientDto patientDto) {
        return new Patient(
//                patientDto.getId(),
                patientDto.getAnimalType(),
                patientDto.getAnimalNick());
    }

    public PatientDto mapToPatientDto(final Patient patient) {
        return new PatientDto(
                patient.getId(),
                patient.getAnimalType(),
                patient.getAnimalNick());
//                patient.getOwnerId());
    }

    public List<PatientDto> mapToPatientDtoList(final List<Patient> patientsList) {
        return patientsList.stream()
                .map(t -> new PatientDto(t.getId(), t.getAnimalType(), t.getAnimalNick()))
                .collect(Collectors.toList());
    }
}
