package com.mr.registration.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OwnerDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String address;
    private int telefonNumber;
}
