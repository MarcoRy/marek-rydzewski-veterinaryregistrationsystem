package com.mr.registration.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "patients")
public class Patient {
    private Long id;
    private String animalType;
    private String animalNick;
    private Owner owner;

    public Patient(String animalType, String animalNick) {
        this.animalType = animalType;
        this.animalNick = animalNick;
    }

    public Patient() {
    }

    @Id
    @GeneratedValue
    @NotNull
    @Column(name = "id", unique = true)
    public Long getId() {
        return id;
    }

    @Column(name = "animal_type")
    public String getAnimalType() {
        return animalType;
    }

    @Column(name = "animal_nick")
    public String getAnimalNick() {
        return animalNick;
    }

    @ManyToOne
    @JoinColumn(name = "owner_id")
    public Owner getOwner() {
        return owner;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public void setAnimalNick(String animalNick) {
        this.animalNick = animalNick;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}

