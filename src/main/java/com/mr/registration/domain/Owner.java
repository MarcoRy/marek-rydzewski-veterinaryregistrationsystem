package com.mr.registration.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "owners")
public class Owner {
    private Long id;
    private String firstName;
    private String lastName;
    private String address;
    private int telefonNumber;
    private List<Patient> patients = new ArrayList<>();

    public Owner() {
    }

    public Owner(Long id, String firstName, String lastName, String address, int telefonNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.telefonNumber = telefonNumber;
    }

    @Id
    @GeneratedValue
    @NotNull
    @Column(name = "id", unique = true)
    public Long getId() {
        return id;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    @Column(name = "telefon_number")
    public int getTelefonNumber() {
        return telefonNumber;
    }

    @OneToMany(
            targetEntity = Patient.class,
            mappedBy = "owner",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    public List<Patient> getPatients() {
        return patients;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTelefonNumber(int telefonNumber) {
        this.telefonNumber = telefonNumber;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
