package com.mr.registration.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PatientDto {
    private Long id;
    private String animalType;
    private String animalNick;
    private Long ownerId;


    public PatientDto(Long id, String animalType, String animalNick) {
        this.id = id;
        this.animalType = animalType;
        this.animalNick = animalNick;
    }
}


