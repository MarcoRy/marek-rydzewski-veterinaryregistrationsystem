package com.mr.registration.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OwnerAndAnimal {
    private Long patientId;
    private Long ownerId;
    private PatientDto patientDto;
}
