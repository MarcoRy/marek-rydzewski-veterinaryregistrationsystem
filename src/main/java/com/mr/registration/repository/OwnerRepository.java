package com.mr.registration.repository;

import com.mr.registration.domain.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface OwnerRepository  extends CrudRepository<Owner,Long>{
    List<Owner>findByTelefonNumber(int telefonNumber);

    @Override
    List<Owner> findAll();

    @Override
    Owner save(Owner owner);

    @Override
    void deleteById(Long id);

    @Override
    Optional<Owner> findById(Long id);
}
