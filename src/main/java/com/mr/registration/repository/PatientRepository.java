package com.mr.registration.repository;

import com.mr.registration.domain.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface PatientRepository extends CrudRepository<Patient, Long> {
    @Override
    List<Patient> findAll();

    @Override
    Patient save(Patient patient);

    @Override
    void deleteById(Long id);

    @Override
    Optional<Patient> findById(Long id);

}