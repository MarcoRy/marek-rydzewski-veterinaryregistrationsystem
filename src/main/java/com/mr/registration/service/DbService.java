package com.mr.registration.service;

import com.mr.registration.domain.Owner;
import com.mr.registration.domain.Patient;
import com.mr.registration.domain.PatientDto;
import com.mr.registration.repository.OwnerRepository;
import com.mr.registration.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DbService {
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private OwnerRepository ownerRepository;

    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    public Optional<Patient> getPatient(Long id) {
        return patientRepository.findById(id);
    }

    public Patient registerPatient(Patient patient) {
        return patientRepository.save(patient);
    }

    public void unregisterPatient(Long id) {
        patientRepository.deleteById(id);
    }


    public List<Owner> getAllOwners() {
        return ownerRepository.findAll();
    }

    public Optional<Owner> getOwner(Long id) {
        return ownerRepository.findById(id);
    }

    public Owner registerOwner(Owner owner) {
        return ownerRepository.save(owner);
    }

    public void unregisterOwner(Long ownerId){
        ownerRepository.deleteById(ownerId);
    }
}

