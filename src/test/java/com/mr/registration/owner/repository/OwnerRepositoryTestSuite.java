package com.mr.registration.owner.repository;

import com.mr.registration.domain.Owner;
import com.mr.registration.domain.Patient;
import com.mr.registration.repository.OwnerRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OwnerRepositoryTestSuite {
    @Autowired
    private OwnerRepository ownerRepository;

    @Test
    public void testOwnerRepositorySaveWithPatients() {
        //Given
        Patient patient1 = new Patient("Snake", "SSSSS");
        Patient patient2 = new Patient("Elephant", "Belinda");

        Owner owner = new Owner(1L,"A", "B", "C", 1);
        owner.getPatients().add(patient1);
        owner.getPatients().add(patient2);

        patient1.setOwner(owner);
        patient2.setOwner(owner);

        //When
        ownerRepository.save(owner);
        Long id = owner.getId();

        //Then
        Assert.assertNotSame(0,id);

        //CleanUp
        ownerRepository.delete(owner);
    }
}